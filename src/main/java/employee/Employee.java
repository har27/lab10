/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employee;

import java.util.Date;

/**
 *
 * @author Harpreet singh
 */
public class Employee {
    
    private String employeeId;
    private Date dateOfJoining;
    
    private double salary;
    
    public Employee( String employeeId , Date dateOfJoining, double salary ){
        this.employeeId = employeeId;
        this.dateOfJoining = dateOfJoining;
        this.salary=salary;
    }
    
    public String getEmployeeId(){
        return employeeId;
    }
         
     public Date getEmployeeDateOfJoining(){
         return dateOfJoining;
     }
    
    public double getEmployeeSalary(){
        return salary;
    }
    
    
}
