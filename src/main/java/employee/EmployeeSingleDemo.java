/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employee;

import java.util.Calendar;

/**
 *
 * @author harpreet Singh
 */
public class EmployeeSingleDemo {
    public static void main( String args[] ){
        Calendar dateJoin = Calendar.getInstance();
        dateJoin.set (2022 ,0 ,22 );
        
        Employee employee = new Employee{"001" ,dateJoin.getTime() , 40000 );
        
        EmployeeTool tool = new EmployeeTool( );
        
        System.out.println("is promotion due "+ tool.isPromotionDueThisYear(employee, true));
        System.out.println("taxes for the year " +tool.calcIncomeTaxForCurrentYear(employee, 0.50));
    
    }
    
}
